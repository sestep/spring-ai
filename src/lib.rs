#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use lazy_static::lazy_static;
use std::ffi::{c_void, CStr, CString};
use std::{collections::HashMap, os::raw::c_int, sync::RwLock};

struct AI {
    id: c_int,
    callback: SSkirmishAICallback,
}

lazy_static! {
    static ref ais_lock: RwLock<HashMap<c_int, AI>> = RwLock::new(HashMap::new());
}

fn log(ai: &AI, message: &str) {
    let cstring = CString::new(message).unwrap();
    unsafe { ai.callback.Log_log.unwrap()(ai.id, cstring.as_ptr()) }
}

#[no_mangle]
pub extern "C" fn init(skirmish_ai_id: c_int, inner_callback: *const SSkirmishAICallback) -> c_int {
    let mut ais = ais_lock.write().unwrap();
    ais.insert(
        skirmish_ai_id,
        AI {
            id: skirmish_ai_id,
            callback: unsafe { *inner_callback },
        },
    );
    let ai = ais.get(&skirmish_ai_id).unwrap();
    log(ai, "Hello from Botsam!");
    0
}

#[no_mangle]
pub extern "C" fn release(skirmish_ai_id: c_int) -> c_int {
    ais_lock.write().unwrap().remove(&skirmish_ai_id);
    0
}

#[no_mangle]
pub extern "C" fn handleEvent(skirmish_ai_id: c_int, topic: c_int, data: *const c_void) -> c_int {
    let ais = ais_lock.read().unwrap();
    let ai = ais.get(&skirmish_ai_id).unwrap();
    if (topic as i64) == (EventTopic_EVENT_UNIT_FINISHED as i64) {
        log(ai, "Unit finished!");
        let event: SUnitFinishedEvent = unsafe { *data.cast() };
        log(ai, &format!("Finished unit: {}", event.unit));
        let unitdef = unsafe { ai.callback.Unit_getDef.unwrap()(skirmish_ai_id, event.unit) };
        log(ai, &format!("Unitdef: {}", unitdef));
        let unitdefname = unsafe { ai.callback.UnitDef_getName.unwrap()(skirmish_ai_id, unitdef) };
        unsafe { ai.callback.Log_log.unwrap()(ai.id, unitdefname) };
        let name = unsafe { CStr::from_ptr(unitdefname) }.to_str().unwrap();
        log(ai, name);
        if name == "armcom" {
            log(ai, "Found the commander!");
            let cstring = CString::new("armsolar").unwrap();
            let solardef =
                unsafe { ai.callback.getUnitDefByName.unwrap()(skirmish_ai_id, cstring.as_ptr()) };
            log(ai, &format!("Solardef: {}", solardef));
            let mut v: Vec<f32> = vec![0.0, 0.0, 0.0];
            let compos = v.as_mut_ptr();
            unsafe { ai.callback.Unit_getPos.unwrap()(skirmish_ai_id, event.unit, compos) };
            log(ai, &format!("Position: vec![{}, {}, {}]", v[0], v[1], v[2]));
            let mut command = SBuildUnitCommand {
                unitId: event.unit,
                groupId: -1,
                options: 0,
                timeOut: 10000,
                toBuildUnitDefId: solardef,
                buildPos_posF3: compos,
                facing: 0,
            };
            let code = unsafe {
                ai.callback.Engine_handleCommand.unwrap()(
                    skirmish_ai_id,
                    COMMAND_TO_ID_ENGINE,
                    -1,
                    CommandTopic_COMMAND_UNIT_BUILD as i32,
                    (&mut command as *mut SBuildUnitCommand).cast(),
                )
            };
            log(ai, &format!("Return code: {}", code));
            return code;
        }
    }
    0
}
