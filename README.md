# Spring AI

Rust AI for Spring RTS.

## Usage

This section does not require you to clone the repository.

### Linux

First, download the latest [artifacts][]:
```
$ wget -O ai.zip https://gitlab.com/sestep/spring-ai/-/jobs/artifacts/master/download?job=linux
```
Then unzip that archive to produce an `AI` folder:
```
$ unzip ai.zip
```
And merge the contents of that `AI` folder with those of the `AI` folder in your
existing Spring installation:
```
$ cp -r AI ~/snap/springlobby-nsg/common/.spring/engine/104.0.1-1510-g89bb8e3
```
Now you should be able to add an AI called "Botsam dev" to a Balanced
Annihilation battle.


## Development

Be sure to clone this repository using `--recurse-submodules`.

### Building locally

First [install Rust][] and clang, then build the bot:
```
$ cargo build
```
This will put the built dynamic library in `target/debug`. Next you can also run
this helper script to gather all the necessary files into an `AI` directory:
```
$ ./gather.sh
```
Now you can install your locally-built version of the bot using the `cp -r`
instruction from the [Usage][] section above.

#### Running the tests

```
$ cargo test
```

#### Building the docs

This will put the built docs in `target/doc`, with a nice entrypoint in
`target/doc/SkirmishAI/index.html`:
```
$ cargo doc
```

### Building the Docker image

First [install Docker][], then build the Docker image:
```
$ docker build --pull -t registry.gitlab.com/sestep/spring-ai:master .
```

---

[artifacts]: https://gitlab.com/sestep/spring-ai/-/jobs/artifacts/master/browse?job=linux
[install docker]: https://docs.docker.com/get-docker/
[install rust]: https://www.rust-lang.org/tools/install
[usage]: #linux
