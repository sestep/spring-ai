#!/usr/bin/env bash
set -e
ARTIFACTS="AI/Skirmish/$(cat name.txt)/$(cat VERSION)"
mkdir -p "$ARTIFACTS"
cp data/*.lua "$ARTIFACTS"
cp target/debug/libSkirmishAI.so "$ARTIFACTS"
