FROM rust:1.46.0

# install non-Rust dependencies
RUN apt-get update && apt-get install -y clang

WORKDIR /spring-ai

# download dependencies
COPY Cargo.lock Cargo.toml ./
RUN mkdir src
RUN echo '#![allow(non_snake_case)]' > src/lib.rs
RUN cargo fetch

# build bot
COPY VERSION build.rs gather.sh name.txt wrapper.h ./
COPY data data
COPY spring spring
COPY src src
RUN cargo build
